# Pheix docker test suite image

This Docker image includes [Rakudo Star](https://rakudo.org/star) v%rakudo%, which is a Raku distribution that includes MoarVM
virtual machine, Raku compiler, a suite of modules and language documentation.

Image contents:

1. Ethereum [Solidity](https://github.com/ethereum/solidity/releases/tag/v%solidity%) compiler (v%solidity% from `ppa:ethereum/ethereum` repository).

2. Geoipupdate %geoipupdate%: https://github.com/maxmind/geoipupdate/releases/tag/v%geoipupdate%

3. ImageMagick %imagemagick%: https://imagemagick.org/archive/releases/

4. Additional Raku modules:
    * [Data::Dump](https://github.com/tony-o/perl6-data-dump)
    * [XML](https://github.com/raku-community-modules/XML/)
    * [GeoIP2](https://github.com/bbkr/GeoIP2)
    * [URI::Encode](https://github.com/raku-community-modules/URI-Encode)
    * [Node::Ethereum::Keccak256::Native](https://gitlab.com/pheix/raku-node-ethereum-keccak256-native)
    * [Net::Ethereum](https://gitlab.com/pheix/net-ethereum-perl6)
    * [Router::Right](https://gitlab.com/pheix/router-right-perl6)
    * [LZW::Revolunet ](https://gitlab.com/pheix/lzw-revolunet-perl6/)
    * [Compress::Bzip2](https://github.com/Altai-man/perl6-Compress-Bzip2)
    * [Compress::Zlib](https://github.com/retupmoca/P6-Compress-Zlib)
    * [FastCGI::NativeCall](https://github.com/jonathanstowe/raku-fastcgi-nativecall)
    * [FastCGI::NativeCall::Async](https://github.com/jonathanstowe/FastCGI-NativeCall-Async/)
    * [MagickWand (fork)](https://github.com/pheix/perl6-magickwand)
    * [HTML::Template (fork)](https://github.com/pheix/html-template)

## Build

You can build an image from this Dockerfile as indicated below:

    $ sudo docker build -t pheix-test-image /path_to_dockerfile/

It will build docker image with the Rakudo Star v%rakudo%. Check out [wiki](https://gitlab.com/pheix-pool/docker-core-perl6/wikis/home) for detailed build instructions.

## Usage

Simply running a container with the image will launch a bash session:

    $ sudo docker run -it pheix-test-image
    root@cc715632ef40:/#

You can also provide `raku` command line switches to `docker run`:

    $ docker run -it pheix-test-image raku -e 'say "Hello!"'

In addition, you can mount a directory from the host within a container and forward the network from host:

    $ docker run -it --net=host -v $HOME/my_raku_projects/:/mount_location/ pheix-test-image /bin/bash

Then, you can run your scripts from inside the container:

    # raku /mount_location/my_raku_script.raku

## Version substitution

HOW-TO for Solidity version retrieval on Fedora: https://gitlab.com/pheix-pool/docker-core-perl6/-/issues/5#note_1051458421

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## Rakudo and Raku language credits

* Project homepage: https://rakudo.org

* Raku modules land: https://raku.land

* GitHub repository: https://github.com/rakudo/star

* Rakudo Star docker: https://github.com/perl6/docker

* Raku Language Specification: https://raku.org/specification/

* Raku Language Documentation: https://docs.raku.org/
